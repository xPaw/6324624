// ==UserScript==
// @version        1.4.1
// @name           Hide watched videos on YouTube
// @icon           https://www.youtube.com/favicon.ico
// @match          https://www.youtube.com/*
// @exclude        https://www.youtube.com/embed/*
// @exclude        https://www.youtube.com/api/*
// @namespace      https://gist.github.com/xPaw/6324624
// @updateURL      https://gist.github.com/xPaw/6324624/raw/YoutubeHideWatched.user.js
// @downloadURL    https://gist.github.com/xPaw/6324624/raw/YoutubeHideWatched.user.js
// @grant          none
// ==/UserScript==

const app = document.querySelector( 'ytd-app' );

function HideVideos( a )
{
	app.querySelectorAll( 'ytd-browse[page-subtype="subscriptions"] ytd-thumbnail-overlay-resume-playback-renderer' ).forEach( element =>
	{
		// Find the container element for this video
		let tagName;

		do
		{
			element = element.parentNode;
			tagName = element.tagName.toLowerCase();
		}
		while( tagName !== 'ytd-grid-video-renderer' && tagName !== 'ytd-item-section-renderer' );

		element.hidden = true;
	} );
}

function ProcessPage()
{
	if( !window.location.pathname.startsWith( '/feed/subscriptions' ) )
	{
		return;
	}

	const list = app.querySelector( 'ytd-section-list-renderer' );

	if( list.dataset.hooked )
	{
		return;
	}

	list.dataset.hooked = true;
	list.addEventListener( 'yt-next-continuation-data-updated', HideVideos );

	// TODO: Find an event to fix this
	new MutationObserver( HideVideos ).observe( list, { childList: true, subtree: true } );
}

if( app )
{
    app.addEventListener( 'yt-navigate-finish', ProcessPage );
}

ProcessPage();
